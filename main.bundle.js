webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"py-5 text-center\">Calculator</div>\n\n  <div class=\"form-group text-center\">\n    <div class=\"row\">\n      <div class=\"col\">\n        <select [(ngModel)]=\"operation\" class=\"form-control\">\n          <option *ngFor=\"let x of operators\" [value]=\"x\" [selected]=\"operation == x\">{{x}}</option>\n        </select>\n      </div>\n      <div class=\"col\">\n        <input [(ngModel)]=\"value\" type=\"number\" class=\"form-control\" placeholder=\"Enter a number\" />\n      </div>\n      <div class=\"col\">\n        <button (click)=\"addStep()\" type=\"button\" class=\"btn btn-primary\">Add Step</button>\n      </div>\n    </div>\n\n    <div class=\"row mt-3 text-left\" *ngIf=\"steps.length > 0\">\n      <div class=\"card col\">\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">Steps:</h5>\n          <ul class=\"list-group card-text\">\n            <ol *ngFor=\"let x of steps\" class=\"list-group-item\">{{x.operation}} {{x.value}}</ol>\n          </ul>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"mt-3 text-left\">\n        <button (click)=\"calculate()\" type=\"button\" class=\"btn btn-primary\">Calculate</button>\n        <button (click)=\"reset()\" type=\"button\" class=\"btn btn-secondary\">Reset</button>\n    </div>\n\n    <div class=\"mt-3 text-left\" *ngIf=\"showResult\">\n      <div class=\"card\">\n        <div class=\"card-body\">\n          Result: <span class=\"badge badge-info\">{{resultAll}}</span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    /**
     * Constructor
     */
    function AppComponent() {
        this.value = null;
        this.steps = [];
        this.resultAll = 0;
        this.showResult = false;
        this.operandList = {
            add: 'Add',
            subtract: 'Subtract',
            multiply: 'Multiply',
            divide: 'Divide',
            apply: 'Apply'
        };
        this.operators = [this.operandList.add,
            this.operandList.subtract,
            this.operandList.multiply,
            this.operandList.divide,
            this.operandList.apply
        ];
        this.operation = this.operandList.add;
    }
    /**
     * Add step
     */
    AppComponent.prototype.addStep = function () {
        var obj = {
            value: this.value,
            operation: this.operation
        };
        this.steps.push(obj);
        this.value = null;
    };
    /**
     * Reset
     */
    AppComponent.prototype.reset = function () {
        this.operation = this.operandList.add;
        this.value = null;
        this.steps = [];
        this.showResult = false;
    };
    /**
     * Calculate operations
     */
    AppComponent.prototype.calculate = function () {
        var _this = this;
        var result = 0;
        this.steps.unshift(this.steps.splice(this.steps.findIndex(function (item) { return item.operation === _this.operandList.apply; }), 1)[0]);
        var _that = this;
        this.steps.forEach(function (step) {
            switch (step.operation) {
                case _that.operandList.add:
                    {
                        result = result + step.value;
                        break;
                    }
                case _that.operandList.subtract:
                    {
                        result = result - step.value;
                        break;
                    }
                case _that.operandList.multiply:
                    {
                        result = result * step.value;
                        break;
                    }
                case _that.operandList.divide:
                    {
                        result = result / step.value;
                        break;
                    }
                case _that.operandList.apply:
                    {
                        result = step.value;
                        break;
                    }
            }
        });
        this.resultAll = result;
        this.showResult = true;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map